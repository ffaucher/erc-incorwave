+++
Title="Welcome"
+++

{{< paragraph-justify >}}
Project INCORWAVE is an [ERC STARTING GRANT](https://erc.europa.eu/apply-grant/starting-grant) 
project funded by the [European Research Council (ERC)](https://cordis.europa.eu/project/id/101116288), [doi:10.3030/101116288](https://doi.org/10.3030/101116288). 
It is led by principal investigator [Florian Faucher]({{< ref "/about" >}} "About Us") in the [INRIA Team Makutu](https://team.inria.fr/makutu/).
{{< paragraph-end >}}


{{< mywelcome >}}

---

## Latest News
