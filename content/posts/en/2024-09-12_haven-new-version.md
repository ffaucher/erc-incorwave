+++
title = 'Hawen version 1.3.1'
date = 2024-09-12T10:10:00+01:00
draft = false
author = 'Florian Faucher'
tags = ['Software', 'Hawen', 'git']
categories = ['Software']
+++

{{< paragraph-justify >}}
A new version of Hawen is available online, see [https://ffaucher.gitlab.io/hawen-website/](https://ffaucher.gitlab.io/hawen-website/).
**Version 1.3.1** includes in particular new options for sources, such as Gaussian sources. It also allows for elastic isotropic and 
anisotropic planewaves, cf. [the dedicated paper]({{< ref "/posts/en/2024-06-14_paper_hdg" >}}).
{{< paragraph-end >}}
