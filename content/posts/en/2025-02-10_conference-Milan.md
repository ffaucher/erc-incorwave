+++
title = 'Inverse Problems conference in Milan'
date = 2025-02-10T10:00:00+01:00
draft = false
author = 'Florian Faucher'
tags = ['Conference']
categories = ['Conference']
+++

We organize a conference on Inverse Problems that will
take place at the University of Milan, from June 9th to 
June 13th, 2025: [https://csc.univie.ac.at/ip_milan_2025/](https://csc.univie.ac.at/ip_milan_2025/).

