+++
title = 'Publication in Transactions on Machine Learning Research '
date = 2024-02-10T10:00:00+01:00
draft = false
author = 'Florian Faucher'
tags = ['Publication', 'Paper']
categories = ['Publication']
+++

Publication accepted in journal Transactions on Machine Learning Research: \
  T. Liu, J. A. Lara Benitez, F. Faucher, A. Khorashadizadeh, M. V. de Hoop, and I. Dokmanic. \
  [WaveBench: Benchmarks Datasets for Modeling Linear Wave Propagation PDEs](https://openreview.net/forum?id=6wpInwnzs8), \
  Transactions on Machine Learning Research, 2835--8856, 2024, [GitHub code](https://github.com/wavebench/wavebench), 
  [Zenodo Repository](https://zenodo.org/records/8015145).

**Abstract**

{{< paragraph-justify >}} 
Wave-based imaging techniques play a critical role in diverse scientific endeavors, 
from discovering hidden structures beneath the Earth’s surface to ultrasound diagnostics. 
They rely on accurate solutions to the forward and inverse problems for partial 
differential equations (PDEs) that govern wave propagation. Surrogate PDE solvers
based on machine learning emerged as an effective approach to computing the solutions
more efficiently than via classical numerical schemes. However, existing datasets for PDE
surrogates offer only limited coverage of the wave propagation phenomenon. In this paper,
we present WaveBench, a comprehensive collection of benchmark datasets for wave 
propagation PDEs. WaveBench (1) contains 24 datasets that cover a wide range of forward
and inverse problems for time-harmonic and time-varying wave phenomena in 2D; (2) includes 
a user-friendly PyTorch environment for comparing learning-based methods; and (3)
comprises reference performance and model checkpoints of popular PDE surrogates such as
U-Nets and Fourier neural operators. Our evaluation on WaveBench demonstrates the
impressive performance of PDE surrogates on in-distribution samples, while simultaneously
unveiling their limitations on out-of-distribution (OOD) samples. This OOD-generalization
limitation is noteworthy, especially since we use stylized wavespeeds and provide abundant
training data to PDE surrogates. We anticipate that WaveBench will stimulate the development 
of accurate wave-based imaging techniques through machine learning.
{{< paragraph-end >}}


