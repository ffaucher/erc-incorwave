+++
title = 'Open positions at INRIA Makutu - University of Pau '
date = 2024-12-22T14:00:00+01:00
draft = false
author = 'Florian Faucher'
tags = ['Position']
categories = ['Position']
+++

Open positions for graduate students are open for 2025, in topics related
to **inverse problems** and **computer sciences**. More information can 
be found on [the dedicated paper]({{< ref "/categories/position/_index.md" >}}).

