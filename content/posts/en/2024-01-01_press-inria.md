+++
title = 'INRIA Press release'
date = 2024-01-08T15:00:00+01:00
draft = false
author = 'Florian Faucher'
tags = ['Publication','Outreach']
categories = ['Publication']
+++

{{< paragraph-justify >}}
INRIA published an article regarding the ERC Starting Grant INCORWAVE and
Principal Investigator Florian Faucher:
https://www.inria.fr/fr/terre-soleil-ondes-sismiques-simulations
{{< paragraph-end >}}
