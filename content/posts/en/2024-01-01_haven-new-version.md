+++
title = 'Hawen version 1.3.0'
date = 2024-01-08T15:10:00+01:00
draft = false
author = 'Florian Faucher'
tags = ['Software', 'Hawen', 'git']
categories = ['Software']
+++

{{< paragraph-justify >}}
A new version of Hawen is available online, see [https://ffaucher.gitlab.io/hawen-website/](https://ffaucher.gitlab.io/hawen-website/).
**Version 1.3.0** includes in particular new high-order quadrature formulas, and a reworking of the elastic wave propagator to better handle 
elastic anisotropy, see [the dedicated paper]({{< ref "/posts/en/2024-06-14_paper_hdg" >}}).
{{< paragraph-end >}}
