+++
title = 'Preprint on helioseismology '
date = 2024-02-02T10:00:00+01:00
draft = false
author = 'Florian Faucher'
tags = ['Publication', 'Preprint']
categories = ['Publication']
+++

A new research report is available online: \
  H. Pham, F. Faucher, D. Fournier, H. Barucq, and L. Gizon.
  [Assembling algorithm for Green's tensors and absorbing boundary conditions 
   for Galbrun's equation in radial symmetry](https://arxiv.org/abs/2401.17080/), 
   arXiv preprint 2401.17080, pp. 1--33, 2024.

**Abstract**

{{< paragraph-justify >}} 
Solar oscillations can be modeled by Galbrun's equation which describes 
Lagrangian wave displacement in a self-gravitating stratified medium. 
For spherically symmetric backgrounds, we construct an algorithm to compute 
efficiently and accurately the coefficients of the Green's tensor of the 
time-harmonic equation in vector spherical harmonic basis. 
With only two resolutions, our algorithm provides values of the kernels 
for all heights of source and receiver, and prescribes analytically the 
singularities of the kernels. 
We also derive absorbing boundary conditions (ABC) to model wave propagation 
in the atmosphere above the cut-off frequency. The construction of ABC, 
which contains varying gravity terms, is rendered difficult by the complex 
behavior of the solar potential in low atmosphere and for frequencies 
below the Lamb frequency. We carry out extensive numerical investigations 
to compare and evaluate the efficiency of the ABCs in capturing outgoing 
solutions. Finally, as an application towards helioseismology, we compute 
synthetic solar power spectra that contain pressure modes as well as 
internal-gravity (g-) and surface-gravity (f-) ridges which are missing 
in simpler approximations of the wave equation. For purpose of validation, 
the location of the ridges in the synthetic power spectra are compared 
with observed solar modes.
{{< paragraph-end >}}
