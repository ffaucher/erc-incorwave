---
title:  "List of Publications"
Author: "Florian Faucher"
---


## Pre-prints
  
- H. Barucq, M. Duprez, F. Faucher, E. Franck, F. Lecourtier, V. Lleras, V. Michel-Dansac, and N. Victorion. \
  [Enriching continuous Lagrange finite element approximation spaces using neural networks](https://arxiv.org/abs/2502.04947), \
  [arXiv preprint](https://arxiv.org/abs/2502.04947).

## Journal articles

- H. Pham, F. Faucher, D. Fournier, H. Barucq, and L. Gizon.\
  [Assembling algorithm for Green's tensors and absorbing boundary conditions 
   for Galbrun's equation in radial symmetry](https://doi.org/10.1016/j.jcp.2024.113444), \
   Journal of Computational Physics, Volume 519, 113444, 2024.
  [arXiv preprint](https://arxiv.org/abs/2401.17080).
   
- H. Pham, F. Faucher, and H. Barucq. \
  [Numerical investigation of stabilization in the Hybridizable Discontinuous Galerkin method for linear anisotropic elastic equation](https://doi.org/10.1016/j.cma.2024.117080), 
  Computer Methods in Applied Mechanics and Engineering, 428 (1), pp. 1--23, 2024. 
  [arXiv preprint](https://arxiv.org/abs/2403.02862), [extended research report](https://hal.archives-ouvertes.fr/hal-04356602/).

- J. A. Lara Benitez, T. Furuya, F. Faucher, A. Kratsios, X. Tricoche and M. V. de Hoop. \
  [Out-of-distributional risk bounds for neural operators with applications to the Helmholtz equation](https://doi.org/10.1016/j.jcp.2024.113168), 
  Journal of Computational Physics, 513, 113168, 2024. [arXiv preprint](https://arxiv.org/abs/2301.11509)


- T. Liu, J. A. Lara Benitez, F. Faucher, A. Khorashadizadeh, M. V. de Hoop, and I. Dokmanic. \
  [WaveBench: Benchmarks Datasets for Modeling Linear Wave Propagation PDEs](https://openreview.net/forum?id=6wpInwnzs8), \
  Transactions on Machine Learning Research, 2835--8856, 2024, [GitHub code](https://github.com/wavebench/wavebench),
  [Zenodo Repository](https://zenodo.org/records/8015145).

&nbsp;

## Outreach
- Presentation of the team activities in helioseismology for the solar eclipse of April 8th 2024: [link FR](https://inria.fr/fr/soleil-geosciences-modelisation-simulation)/[EN](https://inria.fr/en/sun-geosciences-modeling-simulation).
- INRIA presentation of the project INCORWAVE: [link](https://www.inria.fr/fr/terre-soleil-ondes-sismiques-simulations).

&nbsp; 

___

## Latest News on Publications
