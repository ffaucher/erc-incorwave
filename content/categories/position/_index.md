---
title:  "Open Positions"
Author: "Florian Faucher"
---


---	
> Motivated candidates are welcome to contact &nbsp; [florian.faucher .at. inria.fr](mailto:florian.faucher@inria.fr)  &nbsp; regarding opportunities for Post-doctoral, Ph.D. positions, or Master internships, to join the [INRIA Makutu team](https://team.inria.fr/makutu/) at the University of Pau, in the south-west of France. 
---

- **Our Research Interests:** &nbsp; &nbsp;
               •  Inverse problems
&nbsp; &nbsp;  •  Passive imaging 
&nbsp; &nbsp;  •  Data-driven methods
&nbsp; &nbsp;  •  Elastic waves 
&nbsp; &nbsp;  •  Helioseismology and Asteroseismology
&nbsp; &nbsp;  •  High-performance computing
&nbsp; &nbsp;  •  GPUs programming 
&nbsp; &nbsp;  •  AI-enhanced methods

- We are currently focusing on positions related to **mathematical and computational inverse problems**, **data-driven methods**, 
and **high-performance computing with GPUs**, with an emphasis on the Hybridizable Discontinuous Galerkin method. We give below 
a few more details

- The start date is flexible, post-doc and engineer positions are for two years, with the possibility of renewal.

- **Contact:**&nbsp; &nbsp; CV and cover letter must be sent to [florian.faucher .at. inria.fr](mailto:florian.faucher@inria.fr).

---

## Post-doctoral position in Inverse Problems

We are seeking motivated post-doctoral researchers with a strong background in inverse problems to join our team. 
We welcome applicants with expertise in both theoretical and computational aspects, and encourage diverse profiles 
to apply. Our research focuses on areas such as:
- **Bayesian inverse problems and stochastic approaches for quantitative inversion**
- **Convergence and Stability Analysis in Passive Imaging** 
- **Quantitative Imaging Methodologies for Anisotropic media** 

Our research is driven by real-world challenges in solar and Earth imaging, with a particular focus on 
improving our understanding of complex media and advancing imaging technologies. Successful candidates 
will have the opportunity to engage in cutting-edge research that has the potential to make a significant 
impact in both environmental and astronomical studies. 

**Contact:**&nbsp; &nbsp; CV and cover letter must be sent to [florian.faucher .at. inria.fr](mailto:florian.faucher@inria.fr).
    

## Post-doctoral and/or Research Engineer positions in Computer Sciences
  
  We are seeking candidates with a background in High Performance Computing (HPC) to 
  contribute to the development of the open-source software [Hawen](https://ffaucher.gitlab.io/hawen-website/). 
  Specifically, we are currently focusing on two key areas of research: \
  **Topic 1: Development of GPU Kernels:** We are particularly interested in candidates 
       experienced with GPU programming (e.g., using CUDA or OpenMP5). The 
       [Hawen](https://ffaucher.gitlab.io/hawen-website/) 
       software utilizes the HDG discretization method, which is well-suited for 
       GPU optimization due to its reliance on operations involving relatively 
       small dense matrices. \
  **Topic 2: Integration of Neural Network Approaches:** We also aim to explore how neural 
       networks can be incorporated into standard discretization methods to enhance their 
       efficiency. The goal is to combine traditional and AI-based techniques to achieve 
       optimal performance gains.

**Contact:**&nbsp; &nbsp; CV and cover letter must be sent to [florian.faucher .at. inria.fr](mailto:florian.faucher@inria.fr).

___

## Ph.D. Positions

While there is no open Ph.D. position currently opened, 
motivated candidates can contact \
[florian.faucher .at. inria.fr](mailto:florian.faucher@inria.fr).


 - **[CLOSED]** &nbsp; &nbsp;   [Modeling and inversion of the cross-correlation of wave signals in terrestrial and solar seismology](https://jobs.inria.fr/public/classic/fr/offres/2023-06836) 

___

## Master's internship

Master's internship typically lasts 6 months, although the exact format 
depends on the student's home institution. We currently have two open
positions:
 - **[CLOSED]** &nbsp; &nbsp; [GPU kernels for open-source software Hawen](https://jobs.inria.fr/public/classic/fr/offres/2024-08349) 
 - **[CLOSED]** &nbsp; &nbsp; [Modeling and inversion of the cross-correlation of wave signals in terrestrial and solar seismology](https://jobs.inria.fr/public/classic/fr/offres/2023-06836) 
 - **[CLOSED]** &nbsp; &nbsp; [Numerical computation of Green’s kernel and associated layer potential integrals in helioseismology](https://jobs.inria.fr/public/classic/en/offres/2023-06835)

&nbsp; 
