---
title:  "Conferences"
Author: "Florian Faucher"
---

## Upcoming conferences
- [JOSO 2025: Journées Ondes du Sud-Ouest](https://joso-2025.github.io/) \
  March 18th -- 20th, 2025, University of Pau and Pays de l'Adour, France.
- [Inverse Problems Conference](https://csc.univie.ac.at/ip_milan_2025/) \
  June 9th -- 13th, 2025, Milano, Italy.
- [SFB Conference Tomography Across the Scales](https://tomography.univie.ac.at/sfb-conference-tomography-across-scales/) \
  June 16th -- 21st, 2025, Strobl, Austria.

## Past
- [SDO 2025 Science Workshop](https://sdo2025.sdo-workshops.org/).
  February 17th -- 21st, 2025, Boulder, CO.
- [Workshop on Computational methods for Inverse and Ill-posed problems](https://ip24-ucl.github.io/).
  November 7th -- 8th, 2024, UCL, London.
- [IPMS -- Inverse Problems: Modeling and Simulation](https://www.ipms-conference.org/ipms2024/). May 26th -- June 1st, 2024, Malta.
- [Conference Waves 2024](https://waves2024.mps.mpg.de/). June 30th -- July 5th, 2024, Berlin, Germany. 
- [ECCOMAS 2024](https://eccomas2024.org/). June 3rd -- June 7th, 2024, Lisbon, Portugal.


&nbsp; 

___

## Latest Conference News 

