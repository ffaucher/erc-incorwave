---
title:  "Software"
Author: "Florian Faucher"
math: katex
---

## [Hawen](https://ffaucher.gitlab.io/hawen-website/)

{{< paragraph-justify >}}
The software hawen (time-HArmonic Wave modEling and INversion using Hybridizable Discontinuous Galerkin Discretization)
is the cornerstone of the numerical applications of the project. 
It is used to solve **time-harmonic forward and inverse wave problems** (e.g., for visco-acoustic, visco-elastic media) using the Hybridizable Discontinuous Galerkin method.
Parallelism is carried out with both mpi and OpenMP to address large-scale applications of Earth's imaging and helioseismology.
It is open-source, documented, and available via a gitlab repository.
{{< paragraph-end >}}

- [![haven logo](figures/logo-haven.png)](https://ffaucher.gitlab.io/hawen-website/)       &nbsp;&nbsp;&nbsp; &nbsp;  Dedicated website with more infos and tutorials: [https://ffaucher.gitlab.io/hawen-website/](https://ffaucher.gitlab.io/hawen-website/).

- [![gitlab logo](figures/gitlab-logo-only_small.png)](https://gitlab.com/ffaucher/hawen/) &nbsp;&nbsp;&nbsp; &nbsp;  [GitLab Repository](https://gitlab.com/ffaucher/hawen/).

- [![DOI](https://joss.theoj.org/papers/10.21105/joss.02699/status.svg)](https://doi.org/10.21105/joss.02699) &nbsp; &nbsp; [Publication in the Journal of Open-Source Software](https://joss.theoj.org/papers/10.21105/joss.02699).

- [![GNU gplv3](figures/gplv3_small.png)](https://www.gnu.org/licenses/gpl-3.0)       &nbsp;&nbsp;&nbsp; &nbsp;  Distributed under the GNU General Public License v3.0.

- **Illustration**: 

{{< paragraph-center >}}
![anisotropy](figures/elastic-wave-tti-solar_128px.png)
Anisotropic elastic time-harmonic wave propagation, extracted
[the dedicated paper]({{< ref "/posts/en/2024-06-14_paper_hdg" >}}).
From left to right: displacement and stress components, 
respectively $u_x$, $u_z$, $\sigma_{xx}$, $\sigma_{zz}$ and $\sigma_{xz}$.
{{< paragraph-end >}}

&nbsp; 

___

## Latest Software News

