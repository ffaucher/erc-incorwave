---
title:  "About"
Author: "Florian Faucher"
---

___ 

### Principal Investigator

**Florian Faucher** -- [**Personal Website**](https://ffaucher.gitlab.io/cv/)\
Project-team Makutu, Inria \
Bordeaux, Université de Pau\
et des Pays de l'Adour. \
**Contact:**  &nbsp;&nbsp; [florian.faucher .at. inria.fr](mailto:florian.faucher@inria.fr)

___

### Project details

|  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           | ERC Starting Grant |
| ----------- | -----------  |
|**Project**          | **INCORWAVE**:  Nonlinear inversion of correlation waveforms with hierarchical reconstructions  |
|**Keywords**         | *Inverse Problems* -- *Wave equations* -- *Helioseismology* -- *Hybridizable discontinuous Galerkin discretization* -- *Time-harmonic waves* |
|**PI**               | Florian Faucher |
|**Host Institution** | INRIA Bordeaux |
|**Duration**         | 01/01/2024 to 12/31/2028  |


___

### Disclaimer
*This project is funded by the European Union (ERC Project 101116288). 
Views and opinions expressed are however those of the author(s) 
only and do not necessarily reflect those of the European Union 
or the European Research Council Executive Agency (ERCEA). 
Neither the European Union nor the granting authority can be held responsible for them.*
